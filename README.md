## Useful Resources

To learn more about the Libre Food Pantry initative:
- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/)

To learn more about the Bear Necessities Market:
- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/BEAR-Necessities-Market/)

---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
